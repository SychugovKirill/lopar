import Swiper from 'swiper/bundle';
import 'swiper/css/bundle';

const initSwiper = () => {
  const swiper = new Swiper(`.swiper`, {

    // direction: 'vertical',
    loop: true,

    // If we need pagination
    pagination: {
      el: `.swiper-pagination`,
      type: `fraction`,
    },

    // Navigation arrows
    navigation: {
      nextEl: `.swiper-button-next`,
      prevEl: `.swiper-button-prev`,
    },

    // And if we need scrollbar
    scrollbar: {
      el: `.swiper-scrollbar`,
    },
  });
};



export default initSwiper;
