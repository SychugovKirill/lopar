const openMenu = () => {
  const buttonMenu = document.querySelector(`.burger`);
  const headerMenu = document.querySelector(`.header__menu`);

  buttonMenu.addEventListener(`click`, () => {
    headerMenu.classList.toggle(`header__menu--active`);
    buttonMenu.classList.toggle(`burger--active`);
  });
};

export default openMenu;
