<?php
/*
Template Name: Главная страница
*/

get_header();
?>
<main>
    <h1><?=get_field('title') ?></h1>
    <h3><?= get_the_title() ?></h3>
</main>
<?php
get_footer();
